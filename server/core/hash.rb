#!/usr/bin/env ruby
require 'digest'
require 'securerandom'

def md5sum(buf)
  Digest::MD5.new.update(buf)
end
def md5sum_file(fname)
  Digest::MD5.file(FILE_DIR + fname)
end
def sha256sum(buf)
  Digest::SHA256.new.update(buf)
end
def sha256sum_file(fname)
  Digest::SHA256.file(FILE_DIR + fname)
end

def hashpass(pass, salt)
  sha256sum(salt+pass).hexdigest
end

def hash_getsalt
  SecureRandom.hex(32)
end
