#!/usr/bin/env ruby
use Rack::Reloader
require 'pebble_timeline'
require 'timers'
require 'nokogiri'
require 'date'
require 'open-uri'

$CORE_DIR = File.dirname(__FILE__) + "/core/"

require $CORE_DIR+"server.rb"

Appli = RackServer.new
Appli.setup

Appli.iterate
run lambda { |env| Appli.request(env) }
