#!/usr/bin/env ruby

class PDXScraper
  def initialize
    @url = "http://puzzledragonx.com/"
  end
  def setup
  end
  def teardown
  end
  def iterate
    printf("Scraping!\n")
    page = Nokogiri::HTML(open(PDX_URL))

    tz_abbr = scrape_timezone(page)
    scrape_dungeons(page, tz_abbr)
  end

  # Implementation
  def scrape_timezone(page)
    tz_text = page.xpath('//table/tr/td/div[@class="titlebar1"]/h2')[0].children[0]
    /\( ([^\s]*) \)/.match(tz_text)[1]
  end
  def get_dungeon_table(page)
    # Definitely get table for dungeon list
    tbl_anc_path = page.xpath('//table/tr/td/a[@id = "dungeon"]')
    return false if tbl_anc_path == nil

    tbl = tbl_anc_path[0].parent # <td>
                         .parent # <tr>
                         .parent # <table>
  end

  def cook_dungeon(evtdate, evtname, tz_abbr)
    datemain = evtdate
    datemain = datemain.children[0] if datemain.children[0]["title"] != nil

    starttime = datemain.children[0].children[0].to_s
    endtime = datemain.children[2].to_s
    name = evtname.children[0].children[0].to_s
    if evtname.children[3] != nil
      subname = evtname.children[3].children[0].to_s
    elsif evtname.children[2] != nil
      jewels = []
      for child in evtname.children[2].children
        jewels.push /en\/img\/thumbnail\/([0-9]+)\.png/.match(child['data-original'])[1].to_i
      end
      if jewels.count == 1
        case jewels[0]
          when 1325
            subname = "Red"
          when 1326
            subname = "Blue"
          when 1327
            subname = "Green"
          when 1328
            subname = "Yellow"
          when 1329
            subname = "Purple"
        end
        subname += " Jewel"
      else
        subname = "All Jewels"
      end
    else
      subname = ""
    end
    infourl = evtname.children[0]['href']
    return PazudoraDungeon.new(name, subname, starttime, endtime, infourl, tz_abbr)
  end
  def scrape_dungeons(page, tz_abbr)
    tbl = get_dungeon_table(page)
    dungeons = []
    for row in tbl.children
      evtdate = nil
      evtname = nil
      for col in row.children
        case col['class']
          when "eventdate"
            evtdate = col
          when "eventname"
            evtname = col
        end
      end
      if evtdate != nil and evtname != nil
        printf("dungeon scraped!\n")
        dan = cook_dungeon(evtdate, evtname, tz_abbr)
        dungeons.push dan
      end
    end
    for dan in dungeons
      send_dungeon_pin(dan)
    end
  end
end
