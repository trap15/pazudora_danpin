#!/usr/bin/env ruby

require $CORE_DIR+"config.rb"
require $CORE_DIR+"constants.rb"

require $CORE_DIR+"hash.rb"

require $CORE_DIR+"router.rb"

require $CORE_DIR+"svr/info.rb"
require $CORE_DIR+"svr/subscribe.rb"

require $CORE_DIR+"pdx/pazudora.rb"
require $CORE_DIR+"pdx/scraper.rb"
require $CORE_DIR+"pdx/pin_sender.rb"

class RackServer
  attr_reader :router, :scraper
  def initialize
    @scraper = PDXScraper.new
    @router = RackRouter.new
    PebbleTimeline.config.api_key = ENV['PEBBLE_TIMELINE_API_KEY']
  end
  def setup
    @router.add_route(/^\/server\/info/, [Appli, :server_info])
    @router.add_route(/^\/server\/iterate/, [Appli, :force_iterate])
    @router.add_route(/^\/pzdr_dungeon\/subscribe/, [Appli, :subscribe])
    @scraper.setup
  end
  def teardown
    @scraper.teardown
  end
  def iterate
    @scraper.iterate
  end
  def force_iterate(env)
    iterate
    return server_info(env)
  end
  def request(env)
    resp = @router.route_for(env)
    if resp
      return resp.finish
    else
      return [404, {}, ["404 error!"]]
    end
  end
end
