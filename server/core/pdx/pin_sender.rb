#!/usr/bin/env ruby

def pin_strftime(dt)
  dt.to_time.utc.strftime("%FT%TZ")
end

def send_dungeon_pin(dan)
  p dan
  pins = PebbleTimeline::Pins.new

  id = dan.pin_id
  start_time = dan.start_time
  # TODO: Adjust start_time to work for the pin day limit
  while (start_time - DateTime.now).to_i <= -2
    start_time += 1
  end
  time = pin_strftime(start_time)
  duration = dan.end_time.to_time.to_i - start_time.to_time.to_i
  return if duration <= 0

  title = dan.name
  subtitle = dan.subname
  body = sprintf("%s\n\nStamina: %d\nBattles: %d\n", dan.dungeon_name, dan.stamina, dan.battles)

  params = {id: id,
            topics: "pzdr_dungeon",
            time: time,
            duration: duration / 60, # It's in minutes
            layout: {
              type: 'calendarPin',
              tinyIcon: "system://images/DURING_PHONE_CALL",
              title: title,
              body: body,
              backgroundColor: "#8844CC"
            }}
  if subtitle != ""
    params[:layout][:locationName] = subtitle
  end
  p params
  begin
    pins.create(params)
  rescue PebbleTimeline::APIError => reason
    printf("Failed create: %s\n\n\n\n\n", reason)
  end
end
