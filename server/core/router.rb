#!/usr/bin/env ruby
class RackRouter
  def initialize
    @routes = Hash.new { |hash,key| hash[key] = [] }
  end
  def add_route(key, dest)
    @routes[key] = dest
  end
  def route_for(env)
    printf("Routing %s\n", env["PATH_INFO"])
    path = env["PATH_INFO"]
    @routes.each do |k,v|
      case k
        when String
          matched = path == k
        when Regexp
          matched = path =~ k
      end
      next unless matched

      req = Rack::Request.new env
      return v[0].__send__(v[1], req)
    end
    # No route
    return nil
  end
end

