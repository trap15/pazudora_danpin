#!/usr/bin/env ruby

class RackServer
  def server_info(req)
    resp = Rack::Response.new

    remote = req.ip

    resp.write "REMOTE ADDRESS:"+remote+"\n"
    resp
  end
end

