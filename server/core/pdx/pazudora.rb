#!/usr/bin/env ruby

class PazudoraDungeon
  attr_reader :name, :subname
  # Subname handles spirit jewels and "Invade Rate x2" like stuff
  attr_reader :start_time, :end_time # DateTime
  attr_reader :pin_id
  attr_reader :dungeon_name, :stamina, :battles
  def initialize(name_str, subname_str, start_str, end_str, infourl, tz_abbr)
    @name = name_str
    @subname = subname_str
    # Convert HTML-isms back
    @subname.gsub!("&amp;", "&")

    @start_time = date_eval(start_str, tz_abbr)
    @end_time = date_eval(end_str, tz_abbr)
    @pin_id = generate_pin_id
    mission_scrape(infourl)
  end
  def date_eval(str, tz_abbr)
    dt = DateTime.strptime(str+" "+tz_abbr, "%m/%d %H:%M %Z")
    if dt.min == 59
      dt += Rational(59, 24*60*60)
    end
    dt
  end
  def generate_pin_id
    hash_data = @name + " " + @start_time.httpdate
    @pin_id = sha256sum(hash_data).to_s
  end

  def mission_scrape(infourl)
    page = Nokogiri::HTML(open(PDX_URL+infourl))
    @dungeon_name = page.xpath('//div/h1')[0].children[0].to_s
    @dungeon_name.sub!(/ - .*$/, "")
    @stamina = page.xpath('//table/tr/td[@class="value-end"]/span[@class="blue"]')[0].children[0].to_s.to_i
    @battles = page.xpath('//table/tr/td[@class="value-end"]/span[@class="green"]')[0].children[0].to_s.to_i
  end
end
